import request from '@/utils/request'

// 查询客户礼物列表
export function listYmxcustomergiftInfo(query) {
  return request({
    url: '/ymx/ymxcustomergiftInfo/list',
    method: 'get',
    params: query
  })
}

// 查询客户礼物详细
export function getYmxcustomergiftInfo(customerGiftId) {
  return request({
    url: '/ymx/ymxcustomergiftInfo/' + customerGiftId,
    method: 'get'
  })
}

// 新增客户礼物
export function addYmxcustomergiftInfo(data) {
  return request({
    url: '/ymx/ymxcustomergiftInfo',
    method: 'post',
    data: data
  })
}

// 修改客户礼物
export function updateYmxcustomergiftInfo(data) {
  return request({
    url: '/ymx/ymxcustomergiftInfo',
    method: 'put',
    data: data
  })
}

// 删除客户礼物
export function delYmxcustomergiftInfo(customerGiftId) {
  return request({
    url: '/ymx/ymxcustomergiftInfo/' + customerGiftId,
    method: 'delete'
  })
}

// 导出客户礼物
export function exportYmxcustomergiftInfo(query) {
  return request({
    url: '/ymx/ymxcustomergiftInfo/export',
    method: 'get',
    params: query
  })
}