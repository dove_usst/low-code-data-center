package com.yabushan.activiti.tasklistener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
/**
 * 请假流程
 * @author yabushan
 *
 */
public class FinishProcessListener implements ExecutionListener{

	@Override
	public void notify(DelegateExecution execution) throws Exception {

		//流程完成时，修改业务数据表状态
	}

}
