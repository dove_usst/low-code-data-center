package com.yabushan.activiti.tasklistener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

public class GroupTaskListener implements TaskListener{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateTask delegateTask) {
		//获取传入的userId
		String nextDealUser= delegateTask.getVariable("nextDealUser").toString();
		if(nextDealUser!=null){
			String[] userIds=nextDealUser.split(",");
			if(userIds.length==1){
				delegateTask.setAssignee(userIds[0]);
			}else{
				for (String userId : userIds) {
					delegateTask.addCandidateUser(userId);

				}
			}

		}
	}

}
