package com.yabushan.activiti.service;

import org.activiti.engine.form.TaskFormData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormServiceImpl implements FormService{
	@Autowired
	private EngineService engineService;
	@Override
	public TaskFormData queryFormData(String taskId) {
		TaskFormData taskFormData=engineService.getFormService().getTaskFormData(taskId);
		return taskFormData;

	}




}
