package com.yabushan.activiti.domain;

public class NextStepAndUser {

	private String nextStep;
	private String nextUserId;
	private String nextUserName;
	private String nextUserRole = "";
	private String jumpStep;
	public String getNextStep() {
		return nextStep;
	}
	public void setNextStep(String nextStep) {
		this.nextStep = nextStep;
	}
	public String getNextUserId() {
		return nextUserId;
	}
	public void setNextUserId(String nextUserId) {
		this.nextUserId = nextUserId;
	}
	public String getNextUserName() {
		return nextUserName;
	}
	public void setNextUserName(String nextUserName) {
		this.nextUserName = nextUserName;
	}
	public String getNextUserRole() {
		return nextUserRole;
	}
	public void setNextUserRole(String nextUserRole) {
		this.nextUserRole = nextUserRole;
	}
	public String getJumpStep() {
		return jumpStep;
	}
	public void setJumpStep(String jumpStep) {
		this.jumpStep = jumpStep;
	}


}
