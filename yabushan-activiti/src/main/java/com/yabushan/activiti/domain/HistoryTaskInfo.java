package com.yabushan.activiti.domain;

import java.util.List;

import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Comment;

public class HistoryTaskInfo {
	/**
	 * 历史流程任务
	 */
	private HistoricTaskInstance historicTaskInstance;
	/**
	 * 任务变量
	 */
	private String info;

	private List<Comment> taskComment;
	public HistoricTaskInstance getHistoricTaskInstance() {
		return historicTaskInstance;
	}
	public void setHistoricTaskInstance(HistoricTaskInstance historicTaskInstance) {
		this.historicTaskInstance = historicTaskInstance;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public List<Comment> getTaskComment() {
		return taskComment;
	}
	public void setTaskComment(List<Comment> taskComment) {
		this.taskComment = taskComment;
	}



}
