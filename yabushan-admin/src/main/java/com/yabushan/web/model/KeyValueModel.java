package com.yabushan.web.model;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 键值对模型
 *
 * @author ly
 * @date 2020/2/14  10:56
 */
@Slf4j
@Data
public class KeyValueModel {

    String name;
    String value;

}
