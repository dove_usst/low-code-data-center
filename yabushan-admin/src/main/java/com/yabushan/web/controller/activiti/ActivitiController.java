package com.yabushan.web.controller.activiti;

import com.yabushan.activiti.domain.HistoryTaskInfo;
import com.yabushan.activiti.domain.NextStepAndUser;
import com.yabushan.activiti.domain.TaskInfo;
import com.yabushan.activiti.service.*;
import com.yabushan.activiti.util.*;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.utils.StringUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/activiti")
@Slf4j
@Api(tags = "工作流服务")
public class ActivitiController extends BaseController {

	//Logger log = Logger.getLogger(VacationController.class);

	/*
	 * @Inject(name="flowEngineService") private FlowService flowEngineService;
	 */
	@Autowired
	private ProcessDefinitionService processDefinitionService;

	@Autowired
	private ProcessInstanceService processInstanceService;

	@Autowired
	private HistoryQueryService historyQueryService;

	@Autowired
	private FlowUtils flowUtils;
	@Autowired
	private workflowUtilService workflowUtilService;
	@Autowired
	private VacationService vacationService;

	/**
	 * 部署流程定义
	 *
	 * @return
	 */
	@PostMapping("/deploy")
	public RestRspVO<Deployment> deploy(String deploymentName, String DeployKey) {
		RestRspVO<Deployment> result = new RestRspVO<Deployment>();
		String bpmn = "";
		String png = "";
		if (DeployKey.equals(Constant.VACATION_PROCESS)) {
			// 二级经理正职请假流程
			bpmn = Constant.VACATION_FLOW_BPMN;
			png = Constant.VACATION_FLOW_PNG;
		} else if (DeployKey.equals(Constant.DEPUTY_PROCESS)) {
			// 二级经理副职请假流程
			bpmn = Constant.DEPUTY_FLOW_BPMN;
			png = Constant.DEPUTY_FLOW_PNG;
		} else if (DeployKey.equals(Constant.CANCEL_PROCESS)) {
			// 销假流程
			bpmn = Constant.CANCEL_FLOW_BPMN;
			png = Constant.CANCEL_FLOW_PNG;
		} else if (DeployKey.equals(Constant.VISIT_PROCESS)) {
			// 探亲假流程
			bpmn = Constant.VISIT_FLOW_BPMN;
			png = Constant.VISIT_FLOW_PNG;
		} else if (DeployKey.equals(Constant.JOBEVALUATION_PROCESS)) {
			// 协作满意度评分申请流程
			bpmn = Constant.JOBEVALUATION_FLOW_BPMN;
			png = Constant.JOBEVALUATION_FLOW_PNG;
		} else if (DeployKey.equals(Constant.TRIOUTOFPLAN_PROCESS)) {
			// 培训管理计划外培训管理流程
			bpmn = Constant.TRIOUTOFPLAN_FLOW_BPMN;
			png = Constant.TRIOUTOFPLAN_FLOW_PNG;
		} else {
			result.setCode(ErrMsgType.DEPLOY_FAIL.getErrcode());
			result.setMessage(ErrMsgType.DEPLOY_FAIL.getErrmsg());
			return result;
		}
		try {
			processDefinitionService.DeployByInputStream(deploymentName, bpmn, png, Constant.PROCESS_FILE_ROOT_PATH);
			result.setCode(ErrMsgType.DEPLOY_SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.DEPLOY_SUCCESS.getErrmsg());
			// result.setResult(deployment);
		} catch (Exception e) {
			log.error(e.toString());
			result.setCode(ErrMsgType.DEPLOY_FAIL.getErrcode());
			result.setMessage(ErrMsgType.DEPLOY_FAIL.getErrmsg());
		}
		return result;
	}


	/**
	 * 启动流程实例
	 *
	 *            用户ID
	 * @param businessId
	 *            业务主键ID
	 * @return
	 *         http://localhost:9088/leap_demo_app/hello/start?userName=发哥&businessId
	 *         =4
	 */
	@PostMapping("/startFlow")
	public RestRspVO<String> start(@RequestParam(value="processDefinitionKey",required = true)String processDefinitionKey, @RequestParam(value="userId",required = true)String userId, @RequestParam(required = true)String businessId, @RequestParam(required = true)String nextDealUser, String nextNodeName, String Info, String nextStep, String isJumpStep) {
		// if("".equals(nextStep) || "null".equals(nextDealUser) ||
		// null==nextStep){
		if ("1".equals(isJumpStep)) {
			// 如果是跳过步骤，则步骤名字取输入的，这里不变
		} else {
			nextStep = Constant.DEFULT_STEP;
		}

		RestRspVO<String> result = new RestRspVO<String>();
		if (Utils.IsEmpty(userId)) {
			result.setCode(ErrMsgType.START_USER_NULL.getErrcode());
			result.setMessage(ErrMsgType.START_USER_NULL.getErrmsg());
			return result;
		}
		if (Utils.IsEmpty(businessId)) {
			result.setCode(ErrMsgType.START_BUSINESS_NULL.getErrcode());
			result.setMessage(ErrMsgType.START_BUSINESS_NULL.getErrmsg());
			return result;
		}
		// 启动流程实例的同时，设置流程变量，使用流程变量来指定任务的办理人，对应Hello.pbmn文件中的#(userName)
		try {
			// 使用流程定义的Key启动流程实例
			org.activiti.engine.runtime.ProcessInstance processInstance = processInstanceService.StartProcess(userId, businessId, processDefinitionKey);
			// 获取当前任务,并执行
			List<Task> task = processInstanceService.getMyTasks(userId);
			boolean flag = false;
			for (Task t : task) {
				if (t.getProcessInstanceId().equals(processInstance.getProcessInstanceId())) {
					processInstanceService.completeTask(t.getId(), userId, nextDealUser,nextNodeName,nextStep, Info);
					flag = true;
					result.setResult(t.getProcessInstanceId());
				}
				if (flag)
					break;
			}
			result.setCode(ErrMsgType.START_SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.START_SUCCESS.getErrmsg());

		} catch (Exception e) {
			log.error(e.toString());
			result.setCode(ErrMsgType.START_FAIL.getErrcode());
			result.setMessage(ErrMsgType.START_FAIL.getErrmsg());
		}
		return result;
	}

	/**
	 * 获取我的待办任务
	 *
	 * @return
	 */
	@PostMapping("/getAllTask")
	public RestRspVO<List<TaskInfoBean>> queryAllTask(String userId) {
		RestRspVO<List<TaskInfoBean>> result = new RestRspVO<List<TaskInfoBean>>();
		List<TaskInfoBean> taskinfo = new ArrayList<TaskInfoBean>();
		List<Task> list = new ArrayList<Task>();
		if (Utils.IsEmpty(userId)) {
			result.setCode(ErrMsgType.START_USER_NULL.getErrcode());
			result.setMessage(ErrMsgType.START_USER_NULL.getErrmsg());
			return result;
		}
		try {
			// 个人任务
			List<Task> taskList = processInstanceService.queryMySingleTasks(userId);
			if (taskList != null && taskList.size() > 0) {
				list.addAll(taskList);
			}
			// 组任务
			List<Task> groupTaskList = processInstanceService.queryMyGroupTasks(userId);
			if (groupTaskList != null && groupTaskList.size() > 0) {
				list.addAll(groupTaskList);
			}
			for (Task task : taskList) {
				taskinfo.add(flowUtils.returnTaskInfo(task));
			}
			result.setCode(ErrMsgType.SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
			result.setResult(taskinfo);
		} catch (Exception e) {
			log.error(e.toString());
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage(ErrMsgType.Fail.getErrmsg());
		}
		return result;
	}

	/**
	 * 完成我的任务
	 *
	 * @param taskId
	 * @return
	 */
	@PostMapping("/finishMyTask")
	public RestRspVO<String> FinishMyTask(String processInstanceId, String taskId, String userId, String nextDealUserId,String nextNodeName, String stepInfo, String nextSeqFlow) {
		// RestRspVO<List<HistoryTaskInfo>> result =new
		// RestRspVO<List<HistoryTaskInfo>>();
		RestRspVO<String> result = new RestRspVO<String>();
		try {
			// nextSeqFlow=new String(nextSeqFlow.getBytes("utf-8"));
			if (Utils.IsEmpty(nextSeqFlow)) {
				nextSeqFlow = Constant.DEFULT_STEP;
			}
			// 完成任务
			processInstanceService.completeTask(taskId, userId, nextDealUserId, nextNodeName, nextSeqFlow, stepInfo);

			result.setCode(ErrMsgType.SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
			result.setResult(processInstanceId);
			/*
			 * //获取流程记录 List<HistoryTaskInfo> list =
			 * vacationservice.getAllHistoryStep(processInstanceId, false, null,
			 * null); result.setResult(list);
			 */
		} catch (ActivitiObjectNotFoundException e1) {
			log.error(e1.toString());
			result.setCode(ErrMsgType.TASK_NOT_FOUND.getErrcode());
			result.setMessage(ErrMsgType.TASK_NOT_FOUND.getErrmsg());
		} catch (Exception e) {
			log.error(e.toString());
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage(ErrMsgType.Fail.getErrmsg());
		}
		return result;
	}

	/**
	 * 撤回
	 *
	 * @description 当申请人发起流程时，如果第一个审批人未签收该任务，则允许申请人进行撤回操作
	 *              即将第一个审批环节的处理人改为申请人后，再执行处理人的退回操作 1.更换任务的审批人为申请人 2.执行当前任务的退回操作
	 * @param processInstanceId
	 * @param taskId
	 * @param userId
	 * @param stepInfo
	 * @return
	 */
	@PostMapping("/revokeProcess")
	public RestRspVO<String> revokeProcess(String processInstanceId, String userId, String stepInfo) {
		// RestRspVO<List<HistoryTaskInfo>> result =new
		// RestRspVO<List<HistoryTaskInfo>>();
		RestRspVO<String> result = new RestRspVO<String>();
		String taskId = null;
		if (Utils.IsEmpty(userId) || Utils.IsEmpty(processInstanceId)) {
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage("参数传入有空值，请检查！");
			return result;
		}
		// 从历史任务中获取任务ID
		List<HistoricTaskInstance> list = historyQueryService.getHistoricTaskInstances(processInstanceId, false, null, null);
		if (list != null && list.size() > 0) {
			for (HistoricTaskInstance historicTaskInstance : list) {
				if (!(historicTaskInstance.getDeleteReason() + "").equals("completed")) {
					taskId = historicTaskInstance.getId();
					break;
				}
			}
		}
		Task task = processInstanceService.queryTaskInfo(taskId);
		if (task.getTaskDefinitionKey().equals(Constant.FIRST_DEAL_NODE)) {
			// 1.更换任务的处理人为申请人
			processInstanceService.changeAssigneeTask(taskId, userId);
			// 2.执行当前任务做退回操作
			return FinishMyTask(processInstanceId, taskId, userId, userId, "",stepInfo, Constant.BACK_OPERATION_NAME);
		} else {
			// 当前处理节点不是第一个处理人节点，不允许执行撤回动作
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage("该流程不能执行撤回操作！");
			return result;
		}
	}

	/**
	 * 查询流程定义
	 *
	 * @return
	 */
	@PostMapping("/queryDefVersion")
	public RestRspVO<List<ProcessDefinitionBean>> queryDefinition(String definitionKey, String isNew) {
		Boolean isOnlyNew = true;
		if ("false".equals("isNew")) {
			isOnlyNew = false;
		}
		RestRspVO<List<ProcessDefinitionBean>> result = new RestRspVO<List<ProcessDefinitionBean>>();
		List<ProcessDefinitionBean> definitionBeanList = new ArrayList<ProcessDefinitionBean>();
		ProcessDefinitionBean processDefinitionBean = null;
		try {
			List<ProcessDefinition> list = processDefinitionService.queryDefinitionsByDefinitionKey(definitionKey, isOnlyNew);
			// flowEngineService.queryDefinitions();
			if (list != null && list.size() > 0) {
				for (ProcessDefinition processDefinition : list) {
					processDefinitionBean = new ProcessDefinitionBean();
					processDefinitionBean.setProcessDefinitionId(processDefinition.getId());
					processDefinitionBean.setProcessDefinitionKey(processDefinition.getKey());
					processDefinitionBean.setProcessDefinitionName(processDefinition.getName());
					processDefinitionBean.setProcessDefinitionResourceBpmn(processDefinition.getResourceName());
					processDefinitionBean.setProcessDefinitionResourcePng(processDefinition.getDiagramResourceName());
					processDefinitionBean.setProcessDefinitionVersion(processDefinition.getVersion());
					processDefinitionBean.setProcessDeploymentId(processDefinition.getDeploymentId());
					definitionBeanList.add(processDefinitionBean);
				}
			}
			result.setCode(ErrMsgType.SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
			result.setResult(definitionBeanList);
		} catch (Exception e) {
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage(ErrMsgType.Fail.getErrmsg());
			log.error(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * 获取所有节点
	 *
	 * @param processDefId
	 */
	@PostMapping("/getAllFlowNode")
	public RestRspVO<List<StepCodeName>> getAllFlowNode(String processDefId) {
		RestRspVO<List<StepCodeName>> result = new RestRspVO<List<StepCodeName>>();
		try {
			List<StepCodeName> list = workflowUtilService.getJbpmNode(processDefId);
			result.setCode(ErrMsgType.SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
			result.setResult(list);
		} catch (Exception e) {
			log.error(e.toString());
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage(ErrMsgType.Fail.getErrmsg());
		}
		return result;

	}

	/**
	 * 根据TaskId获取当前任务以及下一步处理人
	 *
	 * @param taskId
	 * @return
	 */
	@PostMapping("/getOneTaskInfo")
	public RestRspVO<TaskInfo> getOneTaskInfo(String taskId) {
		RestRspVO<TaskInfo> result = new RestRspVO<TaskInfo>();
		TaskInfo taskInfo = new TaskInfo();
		try {
			// 获取所有流出线
			List<String> outLineList = processInstanceService.queryOutComeListByTaskId(taskId);
			// 获取任务
			Task task = processInstanceService.queryTaskInfo(taskId);
			// 根据当前任务获取下一步任务处理人
			List<NextStepAndUser> nextDealUser =null;// vacationService.getNextStepAndUser(task);
			// 获取当前任务处理人
			List<IdentityLink> identityLinks = processInstanceService.getIdentityLinks(taskId);
			StringBuffer buf = new StringBuffer();
			String currentuserId = null;
			if (identityLinks != null && identityLinks.size() != 0) {
				for (IdentityLink identityLink : identityLinks) {
					buf.append(identityLink.getUserId()).append(Constant.SEPARATOR);
				}
			}
			if (buf != null) {
				currentuserId = buf.toString().substring(0, buf.toString().length() - 1);
				taskInfo.setCurrentDealUserId(currentuserId);
			}
			buf = new StringBuffer();
			if (outLineList != null && outLineList.size() != 0) {
				for (String outLine : outLineList) {
					buf.append(outLine).append(Constant.SEPARATOR);
				}
			}
			if (buf.length() > 0) {
				taskInfo.setOutTransations(buf.toString().substring(0, buf.toString().length() - 1));
			}

			/**
			 * 对二级经理副职请假流程中的正职审批环节做特殊处理
			 * 正职审批后，判断分管领导和公司正总是否是同一个人，同一个人则跳过分管领导审批环节，直接到公司总经理审批
			 */
			if (task.getProcessDefinitionId().contains("DeputyProcess") && task.getTaskDefinitionKey().equals("usertask2")) {
				if (nextDealUser.size() == 2) {
					NextStepAndUser nextStepAndUser1 = nextDealUser.get(0);
					NextStepAndUser nextStepAndUser2 = nextDealUser.get(1);
					if (nextStepAndUser1.getNextUserId().equals(nextStepAndUser2.getNextUserId())) {
						// 跳转到公司总经理审批环节
						if (nextStepAndUser1.getNextStep().equals("公司总经理审批")) {
							nextDealUser.remove(nextStepAndUser2);
							nextStepAndUser1.setJumpStep("1");
							// taskInfo.setOutTransations(nextStepAndUser1.getNextStep());
							// taskInfo.setOutTransations(taskInfo.getOutTransations().replace("公司总经理审批",
							// ""));
						} else {
							nextDealUser.remove(nextStepAndUser1);
							nextStepAndUser2.setJumpStep("1");
							// taskInfo.setOutTransations(nextStepAndUser2.getNextStep());
							// taskInfo.setOutTransations(taskInfo.getOutTransations().replace("公司总经理审批",
							// ""));
						}
						String outLine = taskInfo.getOutTransations().replace("公司总经理审批", "");
						if (outLine.endsWith(",")) {
							outLine = outLine.substring(0, outLine.length() - 1);
						}
						taskInfo.setOutTransations(outLine);

					} else {
						// 流向分管领导审批环节
						if (nextStepAndUser1.getNextStep().equals("公司总经理审批")) {
							nextDealUser.remove(nextStepAndUser1);
						} else {
							nextDealUser.remove(nextStepAndUser2);
						}
						taskInfo.setOutTransations(taskInfo.getOutTransations().replace("公司总经理审批", ""));
					}
				}
			}
			taskInfo.setNextStepAndUser(nextDealUser);
			taskInfo.setTaskInfoBean(flowUtils.returnTaskInfo(task));
			taskInfo.setAssignee(vacationService.getRequestUserId(taskId));
			result.setResult(taskInfo);
			result.setCode(ErrMsgType.SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
		} catch (Exception e) {
			log.error(e.toString());
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage(ErrMsgType.Fail.getErrmsg());
		}
		return result;
	}

	/**
	 * 启动时加载第一步处理人
	 */
	@PostMapping("/getFirstDealUser")
	public RestRspVO<NextStepAndUser> getFirstDealUser(String processDefinitionKey, String userId, String empType, String businessKey) {
		return getFirstDealUserByKey(processDefinitionKey, userId, empType, null);
	}

	/**
	 * 获取第一步处理人
	 */
	@PostMapping("/getFirstDealUserByKey")
	public RestRspVO<NextStepAndUser> getFirstDealUserByKey(String processDefinitionKey, String userId, String empType, String businessKey) {
		RestRspVO<NextStepAndUser> resultRestRspVO = new RestRspVO<NextStepAndUser>();
		NextStepAndUser nextStepAndUser = new NextStepAndUser();
		if(StringUtils.isEmpty(empType)){
			resultRestRspVO.setCode(ErrMsgType.Fail.getErrcode());
			resultRestRspVO.setMessage("请输入节点名称！");
			return resultRestRspVO;
		}
		try {

			nextStepAndUser = vacationService.getFirstUser(processDefinitionKey, userId, empType, businessKey);
			resultRestRspVO.setResult(nextStepAndUser);
			resultRestRspVO.setCode(ErrMsgType.SUCCESS.getErrcode());
			resultRestRspVO.setMessage(ErrMsgType.SUCCESS.getErrmsg());
		} catch (Exception e) {
			resultRestRspVO.setCode(ErrMsgType.Fail.getErrcode());
			resultRestRspVO.setMessage("流程定义不存在");
			log.error(e.getMessage(), e);
		}
		return resultRestRspVO;
	}


	/**
	 * 根据流程实例ID获取所有流程信息
	 *
	 * @param Id
	 *            业务主键ID 获取流程实例ID
	 * @param flag
	 *            1：业务主键ID,2:流程实例ID
	 * @return
	 */
	@PostMapping("/getAllFlowStep")
	public RestRspVO<List<HistoryTaskInfo>> getAllFlowStepById(String Id, String flag) {
		RestRspVO<List<HistoryTaskInfo>> result = new RestRspVO<List<HistoryTaskInfo>>();
		List<HistoryTaskInfo> list = new ArrayList<HistoryTaskInfo>();
		List<HistoricProcessInstance> historicProcessInstances = new ArrayList<HistoricProcessInstance>();
		result.setCode(ErrMsgType.Fail.getErrcode());
		result.setMessage("传入参数不正确！");
		if (Id == null || "".equals(Id)) {
			return result;
		} else {
			if ("1".equals(flag)) {
				// 根据业务主键ID获取历史任务
				// 需要根据业务ID获取所有流程实例
				historicProcessInstances = historyQueryService.getHistoricProcessInstanceByKey(Id, "bid", false, null, null);
				for (HistoricProcessInstance historicProcessInstance : historicProcessInstances) {
					list.addAll(vacationService.getAllHistoryStep(historicProcessInstance.getId(), false, null, null));
				}
			} else if ("2".equals(flag)) {
				// 根据流程实例ID获取任务
				list = vacationService.getAllHistoryStep(Id, false, null, null);
			} else {
				return result;
			}
		}
		result.setResult(list);
		result.setCode(ErrMsgType.SUCCESS.getErrcode());
		result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
		return result;
	}

	// 初始化参数
	public RestRspVO<String> initProcessParam(String processDefId) {
		return null;

	}

	/**
	 * 根据流程实例ID获取历史流程步骤
	 *
	 * @param processInstanceId
	 * @return
	 */
	@PostMapping("/getStep")
	public RestRspVO<List<HistoryTaskInfo>> getAllHistoryStep(String processInstanceId) {
		RestRspVO<List<HistoryTaskInfo>> result = new RestRspVO<List<HistoryTaskInfo>>();
		try {
			// 获取流程记录
			List<HistoryTaskInfo> list = vacationService.getAllHistoryStep(processInstanceId, false, null, null);
			result.setCode(ErrMsgType.SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
			result.setResult(list);
		} catch (Exception e) {
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage(ErrMsgType.Fail.getErrmsg());
			log.error(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * 自定义流程里
	 * 获取指定流程的 我的待办任务
	 * newAtdV1.0
	 * @param userId
	 * @param processInstanceId 流程ID
	 * @return
	 */
	@PostMapping("/getMyTaskByProId")
	public RestRspVO<List<TaskInfoBean>> queryMyTaskByProId(String userId, String processInstanceId) {
		RestRspVO<List<TaskInfoBean>> result = new RestRspVO<List<TaskInfoBean>>();
		List<TaskInfoBean> taskinfo = new ArrayList<TaskInfoBean>();
		if (Utils.IsEmpty(userId)) {
			result.setCode(ErrMsgType.START_USER_NULL.getErrcode());
			result.setMessage(ErrMsgType.START_USER_NULL.getErrmsg());
			return result;
		}
		try {
			// 个人任务
			List<Task> taskList = processInstanceService.queryMySingleTasks(userId,processInstanceId);
			for (Task task : taskList) {
				taskinfo.add(flowUtils.returnTaskInfo(task));
			}
			result.setCode(ErrMsgType.SUCCESS.getErrcode());
			result.setMessage(ErrMsgType.SUCCESS.getErrmsg());
			result.setResult(taskinfo);
		} catch (Exception e) {
			log.error(e.toString());
			result.setCode(ErrMsgType.Fail.getErrcode());
			result.setMessage(ErrMsgType.Fail.getErrmsg());
		}
		return result;
	}
}
