package com.yabushan.web.controller.data;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yabushan.common.utils.StringUtils;
import io.swagger.annotations.Api;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author yabushan
 * @memo 数据中台请求服务
 * @Date 2021/3/28 18:36
 * @Version 1.0
 */
@RestController
@Api(tags = "数据中台请求服务接口")
public class DataController {




    @PostMapping(value = "/data/getMessageRow")
    public String getMessageRow(String ApiCode, String param) {
        if(StringUtils.isEmpty(ApiCode)){
            return "服务编码不能为空!";
        }
        if(StringUtils.isEmpty(param)){
            return "请求参数不能为空";
        }
        String reslt = result(param, ApiCode,"row");
        JSONObject jsonObject = JSON.parseObject(reslt);
        String resultCode = jsonObject.getString("resultCode");
        String resultDesc = jsonObject.getString("resultDesc");
        String exception = jsonObject.getString("exception");
        String columnNames = jsonObject.getString("columnNames");
        String rows = jsonObject.getString("rows");
        ResultUtil resultUtil = new ResultUtil();
        resultUtil.setException(exception);
        resultUtil.setColumnNames(columnNames);
        resultUtil.setResultCode(resultCode);
        resultUtil.setRows(rows);
        return toJson(resultUtil);
    }

    @PostMapping(value = "/data/getMessageList")
    public String getMessageList(String ApiCode, String param) {
        if(StringUtils.isEmpty(ApiCode)){
            return "服务编码不能为空!";
        }
        if(StringUtils.isEmpty(param)){
            return "请求参数不能为空";
        }
        String reslt = result(param, ApiCode,"list");
        JSONObject jsonObject = JSON.parseObject(reslt);
        String resultCode = jsonObject.getString("resultCode");
        String resultDesc = jsonObject.getString("resultDesc");
        String exception = jsonObject.getString("exception");
        String columnNames = jsonObject.getString("columnNames");
        String rows = jsonObject.getString("rows");
        ResultUtil resultUtil = new ResultUtil();
        resultUtil.setException(exception);
        resultUtil.setColumnNames(columnNames);
        resultUtil.setResultCode(resultCode);
        resultUtil.setRows(rows);
        return toJson(resultUtil);
    }


    public String result(String param, String apiCode,String rowType) {
        try {
            return null;
        } catch (Exception e) {
            return e.toString();
        }

    }


    public String toJson(Object obj) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().disableHtmlEscaping().create();
        String strReturn = gson.toJson(obj);
        return strReturn;
    }
}
