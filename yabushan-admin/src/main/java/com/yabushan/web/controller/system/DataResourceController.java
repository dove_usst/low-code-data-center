package com.yabushan.web.controller.system;

import java.util.ArrayList;
import java.util.List;

import com.yabushan.generator.domain.GenTable;
import com.yabushan.generator.service.IGenTableColumnService;
import com.yabushan.generator.service.IGenTableService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.DataResource;
import com.yabushan.system.service.IDataResourceService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

import javax.xml.crypto.Data;

/**
 * 数据资源Controller
 *
 * @author yabushan
 * @date 2021-01-16
 */
@RestController
@RequestMapping("/system/resource")
public class DataResourceController extends BaseController
{
    @Autowired
    private IDataResourceService dataResourceService;
    @Autowired
    private IGenTableService genTableService;

    @Autowired
    private IGenTableColumnService genTableColumnService;

    /**
     * 查询数据资源列表
     */
    @PreAuthorize("@ss.hasPermi('system:resource:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataResource dataResource)
    {
        startPage();
        List<DataResource> list = dataResourceService.selectDataResourceList(dataResource);
        return getDataTable(list);
    }

    /**
     * 导出数据资源列表
     */
    @PreAuthorize("@ss.hasPermi('system:resource:export')")
    @Log(title = "数据资源", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DataResource dataResource)
    {
        List<DataResource> list = dataResourceService.selectDataResourceList(dataResource);
        ExcelUtil<DataResource> util = new ExcelUtil<DataResource>(DataResource.class);
        return util.exportExcel(list, "resource");
    }

    /**
     * 获取数据资源详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:resource:query')")
    @GetMapping(value = "/{resourceId}")
    public AjaxResult getInfo(@PathVariable("resourceId") String resourceId)
    {
        return AjaxResult.success(dataResourceService.selectDataResourceById(resourceId));
    }

    /**
     * 新增数据资源
     */
    @PreAuthorize("@ss.hasPermi('system:resource:add')")
    @Log(title = "数据资源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody String resourceIds)
    {

        String[] res=resourceIds.split(",");
        int i=dataResourceService.insertDataResource(res);
        if(i>0){
            return AjaxResult.success("操作成功");
        }else{
            return AjaxResult.error("已经没有需要抓取的数据表啦");
        }
    }

    /**
     * 修改数据资源
     */
    @PreAuthorize("@ss.hasPermi('system:resource:edit')")
    @Log(title = "数据资源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataResource dataResource)
    {
        return toAjax(dataResourceService.updateDataResource(dataResource));
    }

    /**
     * 删除数据资源
     */
    @PreAuthorize("@ss.hasPermi('system:resource:remove')")
    @Log(title = "数据资源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{resourceIds}")
    public AjaxResult remove(@PathVariable String[] resourceIds)
    {
        return toAjax(dataResourceService.deleteDataResourceByIds(resourceIds));
    }

    /**
     * 构建数据资源
     */
    @PreAuthorize("@ss.hasPermi('system:resource:add')")
    @Log(title = "构建数据资源", businessType = BusinessType.INSERT)
    @PostMapping("/build/{resourceIds}")
    public AjaxResult build(@PathVariable String[] resourceIds)
    {
        List<DataResource> result = new ArrayList<>();
        List<GenTable> genTables = new ArrayList<>();
        GenTable genTable=null;
        for(int i=0;i<resourceIds.length;i++){
            DataResource dataResource = dataResourceService.selectDataResourceById(resourceIds[i]);
            genTable = new GenTable();
            genTable.setOptions(dataResource.getResourceId());
            genTable.setTableName(dataResource.getResourceName());
            genTable.setTableComment(dataResource.getResourceName());
            genTable.setCreateTime(dataResource.getCreateTime());
            genTable.setUpdateTime(dataResource.getUpdateTime());
            genTables.add(genTable);
        }

        genTableService.importGenTableYun(genTables);
        return AjaxResult.success("操作成功！");
    }
}
