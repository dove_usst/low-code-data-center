package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxDiscountCodeInfo;
import com.yabushan.system.service.IYmxDiscountCodeInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 折扣与保修信息Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxdiscountcodeinfo")
public class YmxDiscountCodeInfoController extends BaseController
{
    @Autowired
    private IYmxDiscountCodeInfoService ymxDiscountCodeInfoService;

    /**
     * 查询折扣与保修信息列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxdiscountcodeinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxDiscountCodeInfo ymxDiscountCodeInfo)
    {
        startPage();
        List<YmxDiscountCodeInfo> list = ymxDiscountCodeInfoService.selectYmxDiscountCodeInfoList(ymxDiscountCodeInfo);
        return getDataTable(list);
    }

    /**
     * 导出折扣与保修信息列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxdiscountcodeinfo:export')")
    @Log(title = "折扣与保修信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxDiscountCodeInfo ymxDiscountCodeInfo)
    {
        List<YmxDiscountCodeInfo> list = ymxDiscountCodeInfoService.selectYmxDiscountCodeInfoList(ymxDiscountCodeInfo);
        ExcelUtil<YmxDiscountCodeInfo> util = new ExcelUtil<YmxDiscountCodeInfo>(YmxDiscountCodeInfo.class);
        return util.exportExcel(list, "ymxdiscountcodeinfo");
    }

    /**
     * 获取折扣与保修信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxdiscountcodeinfo:query')")
    @GetMapping(value = "/{discountCodeId}")
    public AjaxResult getInfo(@PathVariable("discountCodeId") String discountCodeId)
    {
        return AjaxResult.success(ymxDiscountCodeInfoService.selectYmxDiscountCodeInfoById(discountCodeId));
    }

    /**
     * 新增折扣与保修信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxdiscountcodeinfo:add')")
    @Log(title = "折扣与保修信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxDiscountCodeInfo ymxDiscountCodeInfo)
    {
        return toAjax(ymxDiscountCodeInfoService.insertYmxDiscountCodeInfo(ymxDiscountCodeInfo));
    }

    /**
     * 修改折扣与保修信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxdiscountcodeinfo:edit')")
    @Log(title = "折扣与保修信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxDiscountCodeInfo ymxDiscountCodeInfo)
    {
        return toAjax(ymxDiscountCodeInfoService.updateYmxDiscountCodeInfo(ymxDiscountCodeInfo));
    }

    /**
     * 删除折扣与保修信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxdiscountcodeinfo:remove')")
    @Log(title = "折扣与保修信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{discountCodeIds}")
    public AjaxResult remove(@PathVariable String[] discountCodeIds)
    {
        return toAjax(ymxDiscountCodeInfoService.deleteYmxDiscountCodeInfoByIds(discountCodeIds));
    }
}
